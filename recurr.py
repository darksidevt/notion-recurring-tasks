from dataclasses import astuple
import secrets
from notion_client import Client
from datetime import datetime
from datetime import date
import calendar

notion = Client(auth=secrets.KEY)

#Function to decode the JSON output
def dictionary_decoder(x):
    return [i for i in x]

listDBDone = notion.databases.query(database_id=secrets.DATABASE_ID
)

def changeCheckBox(index, state):
    pageId = listDBDone["results"][index]["id"]
    notion.pages.update(page_id=pageId, 
    **{
        "properties":{
            "Done":{
                "checkbox": state
            }
        }
    }
    )
def updateIfDayofWeek(index):
    days = listDBDone["results"][index]["properties"]["Which DOW (Weekly)"]["multi_select"]
    for i in range(0, len(days)):
        dayCheck = listDBDone["results"][index]["properties"]["Which DOW (Weekly)"]["multi_select"][i]["name"]
        if dayCheck == "Only on Day":
            changeCheckBox(index, True)
        if dayCheck == calendar.day_name[date.today().weekday()]:
            return changeCheckBox(index, False)

def updateIfDayofMonth(index):
    day = listDBDone["results"][index]["properties"]["Which day (Day of Month)"]["number"]
    if day == datetime.today().day:
        changeCheckBox(index, False)

def run():
    lengthOfChecked = len(listDBDone["results"])
    for i in range(0, lengthOfChecked):
        curTaskType = listDBDone["results"][i]["properties"]["Type of Task"]["select"]["name"]
        if curTaskType == "Weekly":
            updateIfDayofWeek(i)
        elif curTaskType == "Daily":
            changeCheckBox(i, False)
        elif curTaskType == "Day of Month":
            updateIfDayofMonth(i)

run()