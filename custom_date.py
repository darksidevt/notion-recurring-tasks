from datetime import datetime

class Date():
    def __init__(self,curDate):
        self.curDate = curDate
    def dayOfYear(self):
        updatedDate = datetime.strptime(self.curDate, "%m/%d/%Y").date()
        dayOfYear = updatedDate.timetuple().tm_yday
        return dayOfYear
    def monthNotion(self):
        updatedDate = datetime.strptime(self.curDate, "%m/%d/%Y").date()
        dateOut = f"{updatedDate.year}-{updatedDate.month}-{updatedDate.day}"
        return dateOut
    def monthHuman(self):
        return self.curDate