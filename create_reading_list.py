import secrets
from notion_client import Client
from datetime import datetime
import math
from custom_date import Date

notion = Client(auth=secrets.KEY)

def dictionary_decoder(x):
    return [i for i in x]
    
def convertDate(curDate):
    updatedDate = datetime.strptime(curDate, "%m/%d/%Y").date()
    dateOut = f"{updatedDate.year}-{updatedDate.month}-{updatedDate.day}"
    #print(dateOut)
    dayOfYear = updatedDate.timetuple().tm_yday
    #print(dayOfYear)
    return (dateOut, dayOfYear)

def getPagesPerDay(startDay, endDay, totalPages):
    difference = (endDay - startDay + 1)
    print("THis is the difference ",difference)
    if difference < totalPages:
        pagesPerDay = (totalPages / difference)
        print("hit less than total")
        return pagesPerDay
    if difference <= 0:
        pagesPerDay = (365 - startDay) + endDay / totalPages
        print("hit <=0")
        return pagesPerDay
    if difference == 1:
        return("Those are the same date.")
    else:
       pagesPerDay = (difference / totalPages)
       print("hit other")
       return pagesPerDay

def createPage(date):
    notion.pages.create(
        **{
            "parent": {
                "database_id": databaseID,
            },
            "properties": {
                "Name": {
                    "title": [
                        {
                            "text": {
                                "content": 'Well Read Mommy',
                            },
                        },
                    ],
                },
                "Date": {
                    "date": {
                        "start": date
                    },
                },
            },
        },
    )
def run():
    # databaseID = input("What is the database ID?")
    # startDate = Date(input("What is the start date? (dd/mm/yyyy)"))
    # endDate = Date(input("WHat is the end date? (mm/dd/yyyy)"))
    # numOfPages = int(input("How many pages are there total?"))
    databaseID = "2dd202ab8982429091e665b1125b172f"
    startDate = Date("12/1/2022")
    endDate = Date("12/30/2022")
    numOfPages = 14
    totalPages = getPagesPerDay(startDate.dayOfYear(),endDate.dayOfYear(),numOfPages)
    print(totalPages)


# pagesPerDay = math.ceil((endInfo[1]-startInfo[1] + 1) / numOfPages)
# print(pagesPerDay)

run()
